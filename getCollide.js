
const radius = 1.5;
const radius_z = 1.5;
const height = 3;
const PI = Math.PI;

class Vector {
    constructor(x, y, z, vec) {
        this.x = x;
        this.z = z;
        this.h = y;
        this.vec = {x:vec.x, z:vec.z} || null;
    }

    diffw(pos2){
        return Math.sqrt(
            (Math.pow((pos2.x - this.x), 2) + Math.pow((pos2.z - this.z), 2))
        );
    }

    diffh(h2){
        return Math.abs((h2.h - this.h));
    }

    normal(){
        if(this.vec) {
            var len = Math.sqrt(this.vec.x * this.vec.x + this.vec.z * this.vec.z);
            return {
                x: this.x / len,
                z: this.z / len
            };
        } else return null;
    }

    dot(obj){
        if(this.vec && obj.vec)
            return this.vec.x * obj.vec.x + this.vec.z + obj.vec.z;
        else
            return null;
    }

    ndot(obj){
        if(this.vec && obj.vec)
            return this.normal().x * obj.normal().x + this.normal().z * obj.normal().z;
        else
            return null;
    }

    static dot(vec1, vec2){
        if(vec1 && vec2)
            return vec1.x * vec2.x + vec1.z * vec2.z;
        else
            return null;
    }

    static cross2D(veca, vecb){
        if(veca && vecb)
            return veca.x * vecb.z - vecb.x * veca.z;
        else
            return null;
    }


    static normalizePos(a, b){
        var xx = a.x - b.x;
        var zz = a.z - b.z;
        var len = Math.sqrt((xx*xx + zz*zz));
        return {
            x: xx/len,
            z: zz/len
        };
    }
}


module.exports = function (game, player, monster) {
    var col = false;
    var hit = false;
    var vec;
    // console.log(player.dummySkin, monster.dummySkin);
    var playerVec = {x:game.cameraVector()[0], z:game.cameraVector()[2]};
    var mv = {x:player.position.x - monster.position.x, z:player.position.z - monster.position.z};
    var User = new Vector(player.position.x, player.position.y, player.position.z, playerVec);
    var Mons = new Vector(monster.position.x, monster.position.y, monster.position.z, mv);

    var W = Vector.normalizePos(User, Mons);
    var L = User.vec;
    // console.log(W, L);
    var theta = Math.acos(Vector.dot(W, L));

    if(player.dummySkin !== undefined && monster.dummySkin !== undefined){ // use dummySkin to detect if its zombies' collision
        if (User.diffw(Mons) < radius_z && User.diffh(Mons) < height) {
            col = true;
        }
    }
    else {
        if (User.diffw(Mons) < radius && User.diffh(Mons) < height) {
            col = true;
        }

        var stb = (User.diffw(Mons) < radius+1 && User.diffh(Mons) < height); // PI/4 arc sight in which player can hit zombie
        if(stb && (PI - theta) < PI/8){
            hit = true;
        }
    }

    // console.log(theta/PI * 180);
    var Clockwise = (Vector.cross2D(User.vec, Mons.vec) < 0);
    // console.log(Clockwise);
    if(!Clockwise){
        vec = {
            x: Math.cos(PI/2 - theta),
            z: -Math.sin(PI/2 - theta)
        };
    } else {
        vec = {
            x: Math.cos(PI/2 + theta),
            z: -Math.sin(PI/2 + theta)
        };
    }

    return {
        col: col,
        hit: hit,
        vec: vec,
        theta: PI - theta
    };
};