module.exports = function (game, highlighter, registry) {

    var currentMaterial;
    var light_pnt = 0;
    var n = 20;
    var PointLight = new Array();
    for (var i = 0; i < n; i++) {
        PointLight.push(new game.THREE.PointLight({
            color: '#ffff00',
            distance: 4, // 光線照亮距離
            decay: 2
        }));
        PointLight[i].position.set(1000, 0, 1000)
        PointLight[i].intensity = 0;
        game.scene.add(PointLight[i])

        //game.scene.remove(PointLight[i])
    }
    // window.addEventListener('keydown', function (ev) {
    //     if (ev.which === 'R'.charCodeAt(0)) {
    //         console.log("trigger")
    //         //game.scene.remove(PointLight[0])
    //         for (var i = 0; i < n; i++){
    //             game.scene.remove(PointLight[i])//
    //         }    

    //     }
    // });
    container.addEventListener("mouseup", function (event) {
        var selection = document.getElementById('selection');      
        var num = selection.style.left;
        num = num.replace("%", "");
        num = Number(num);
        num = num/11 + 1;
        var it = document.getElementById('item' + num).src;
        it = it.split("/");
        it = it[it.length - 1].replace(".png", "");
        currentMaterial = registry.blockName2Index[it];
        c = 0;
        // var currentMaterial = 10
        if (event.button === 0) {
            clearInterval(eventNumber2);

        } else if (event.button === 2) {
            var position = highlighter.currVoxelAdj;
            // console.log(highlighter);        
            // 設定光源位置
            if (position) {
                if (currentMaterial == 10) {
                    if (light_pnt == 20) light_pnt = 0;
                    PointLight[light_pnt].position.set(position[0], position[1] + 1, position[2])
                    PointLight[light_pnt].intensity = 0.5;
                    // // 設定光源目標
                    game.scene.add(PointLight[light_pnt])
                    light_pnt++;
                }
            }
        }
    });
    var c = 0;
    var eventNumber2;
    var val = 0;
    container.addEventListener("mousedown", function (event) {
        if (event.button === 0) {
            eventNumber2 = setInterval(function () {
                //console.log("dig " + b);
                // console.log(c)
                if(c === 9) val = game.raycast().value;
                if (c === 10) {
                    clearInterval(eventNumber2);
                    var position = highlighter.currVoxelPos;
                    if (position) {
                        if (val == 10) {

                            for (var i = 0; i < n; i++) {

                                if (position[0] == PointLight[i].position.x && position[1] == PointLight[i].position.y - 1 && position[2] == PointLight[i].position.z) {

                                    PointLight[i].position.set(1000, 1000, 1000)
                                    game.scene.remove(PointLight[i])
                                    break;
                                }
                            }
                        }
                    }
                    return;
                }
                c++;
            }, 500)
        }
    });
}