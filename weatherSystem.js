//voxel snow, material
module.exports = function (thisgame) {
    var snow_flag = false;
    var game = thisgame;
    var snow = require('./snow.js')({
        game: game,
        count: 1000,
        size: 20
    });

    game.on('tick', function () {
        if (snow_flag) snow.tick();
        else snow.dead();
    });

    var rain_flag = false;
    var rain = require('./rain.js')({
        game: game,
        count: 3000,
        size: 10
    });

    game.on('tick', function () {
        if (rain_flag) rain.tick();
        else rain.dead();
    });

    window.addEventListener('keydown', function (ev) {
        if (ev.keyCode === 'M'.charCodeAt(0)) {
            if (snow_flag) {
                game.materials.materials[0] = ["grass_dirt", "grass_dirt", "grass", "dirt", "grass_dirt", "grass_dirt"]
                snow_flag = false
            } else {
                game.materials.materials[0] = ["grass_dirt", "grass_dirt", "whitewool", "dirt", "grass_dirt", "grass_dirt"]
                snow_flag = true
            }
            game.showAllChunks();
            if (rain_flag) rain_flag = false
        }
        if (ev.keyCode === 'N'.charCodeAt(0)) {
            rain_flag = !rain_flag
            if (snow_flag) {
                game.materials.materials[0] = ["grass_dirt", "grass_dirt", "grass", "dirt", "grass_dirt", "grass_dirt"]
                snow_flag = false
                game.showAllChunks();
            }
        }
        if (ev.keyCode === 'P'.charCodeAt(0)) {
            console.log(game.materials)
        }
    })

    var clouds = require('voxel-clouds')({
        game: game,
        high: 12,
        distance: 300,
        many: 100,
        speed: 0.01,
        // material of the clouds
        material: new game.THREE.MeshBasicMaterial({
            emissive: 0xffffff,
            shading: game.THREE.FlatShading,
            fog: false,
            transparent: true,
            opacity: 0.5,
        }),
    });
    game.on('tick', clouds.tick.bind(clouds));
}