var SimplexNoise = require('simplex-noise');
var hash = require('ndhash')

module.exports = function () {
    var waterArr = [];

    var simplex = new SimplexNoise();

    var terrainXZ = 80,
        terrainY = 10

    var waterLevel = -2

    var floor = Math.floor

    return function generateChunk(position, chunkSize) {
        var chunk = new Int8Array(chunkSize * chunkSize * chunkSize);
        var waterChunk = new Int8Array(chunkSize * chunkSize * chunkSize);
        var startX = position[0] * chunkSize
        var startY = position[1] * chunkSize
        var startZ = position[2] * chunkSize

        for (var x = startX; x < startX + chunkSize; ++x) {
            for (var z = startZ; z < startZ + chunkSize; ++z) {
                // simple heightmap across x/z
                var cx = x / terrainXZ
                var cz = z / terrainXZ
                var height = terrainY * simplex.noise2D(cx, cz) >> 0;
                // height -= 3;
                var id = 0;
                for (var y = startY; y < startY + chunkSize; ++y) {
                    id = decideBlockID(x, y, z, height);
                    // setChunk(x, y, z, chunkSize, chunk, id);
                    if (id === 5) {
                        setChunk(x, y, z, chunkSize, chunk, 0);
                        setChunk(x, y, z, chunkSize, waterChunk, 5);
                    } else {
                        setChunk(x, y, z, chunkSize, chunk, id);
                        setChunk(x, y, z, chunkSize, waterChunk, 0);
                    }
                    
                }
                // possibly add a tree at this x/z coord
                tree(chunk, x, startY, z, height, chunkSize, x - startX, z - startZ);

            }
        }
        return [chunk, waterChunk];
    }

    function setChunk(x, y, z, chunkSize, chunk, id) {
        var xidx = Math.abs((chunkSize + x % chunkSize) % chunkSize)
        var yidx = Math.abs((chunkSize + y % chunkSize) % chunkSize)
        var zidx = Math.abs((chunkSize + z % chunkSize) % chunkSize)
        var idx = xidx + yidx * chunkSize + zidx * chunkSize * chunkSize
        if (chunk[idx] === 0) {
            chunk[idx] = id;
        }

    }

    function decideBlockID(x, y, z, groundLevel) {
        // y at or below ground level
        if (y < groundLevel) return 4 //stoneID
        if (y == groundLevel) {
            if (y < 0) return 3 //sandID
            if (y == 0) return 2 //dirtID
            if (y == 1) return 1 //grassID
            return 1
        }

        // pools of water at low level
        if (y < waterLevel) {
            // waterArr.push([x, y, z]);
            return 5 //waterID
        }
        // otherwise air
        return 0
    }

    function tree(chunk, x, yoff, z, height, chunkSize, i, k) {

        var xidx = Math.abs((chunkSize + x % chunkSize) % chunkSize)
        var yidx = Math.abs((chunkSize + height % chunkSize) % chunkSize)
        var zidx = Math.abs((chunkSize + z % chunkSize) % chunkSize)
        var idx = xidx + yidx * chunkSize + zidx * chunkSize * chunkSize
        if (chunk[idx] !== 1) return;

        var rd = Math.random();
        if (rd > 0.05) return;
        // console.log(rd);

        // don't build at chunk border for now
        var border = 5
        if (i < border || k < border) return
        var is = chunkSize
        var ks = chunkSize
        if (i > is - border || k > ks - border) return

        // build the treetrunk
        var js = chunkSize;
        var treelo = height
        var treehi = treelo+ 6 + floor(30 * rd);
        for (var i = treelo+1; i < treehi; ++i) {
            var y = i-yoff
            if (y<0 || y>=js) continue
            setChunk(x, i, z, chunkSize, chunk, 7);
        }

        // spherical-ish foliage
        for (var ci = -3; ci <= 3; ++ci) {
            for (var cj = -3; cj <= 3; ++cj) {
                for (var ck = -3; ck <= 3; ++ck) {
                    var tj = treehi + cj - yoff;
                    if (ci === 0 && ck === 0) continue
                    if (tj<0 || tj>=js) continue
                    var rad = ci * ci + cj * cj + ck * ck
                    if (rad > 15) continue
                    if (rad > 5) {
                        if (rad*hash(x+z+tj,ci,ck,cj) < 6) continue;
                    }
                    // chunk.set(i + ci, tj, k + ck, leafID);
                    setChunk(x + ci, tj, z + ck, chunkSize, chunk, 8);
                }
            }
        }
    }

}