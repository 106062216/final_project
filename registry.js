"use strict";
exports.__esModule = true;
var Registry = /** @class */ (function () {
    function Registry(material) {
        this.blockName2Index = { air: 0 };
        this.blockIndex2Name = ['air'];
        this.material = material;
        for (var i = 0; i < this.material.length; i++) {
            var obj = this.material[i];
            if (typeof obj == 'string') {
                this.blockIndex2Name.push(obj);
                this.blockName2Index[obj] = i + 1;
                // console.log(obj);
            }
            else {
                this.blockIndex2Name.push(obj[2]);
                this.blockName2Index[obj[2]] = i + 1;
                // console.log(obj[2]);
            }
        }
        // console.log(this.blockIndex2Name, this.blockName2Index);
    }
    return Registry;
}());
exports.Registry = Registry;
