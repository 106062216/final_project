declare function require(name:string);
var voxelMesh = require('voxel-mesh');
var voxel = require('voxel');

export class Water {
    game: any;
    material: any;
    constructor(opts) {
        this.game = opts.game;
        this.material = opts.material;
    }

    generate(waterChunk) {
        var game = this.game;
        var scale = new game.THREE.Vector3(1, 1, 1);
        var water = voxelMesh(waterChunk, voxel.meshers.culled, scale, game.THREE);
        water.createSurfaceMesh(this.material);
        water.addToScene(game.scene);

    }

}
