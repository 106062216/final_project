export class Registry {
    blockName2Index: any = { air:0 };
    blockIndex2Name: any = ['air'];
    material: any;
    constructor(material) {
        this.material = material;
        for (let i=0; i<this.material.length; i++) {
            let obj = this.material[i];
            if (typeof obj == 'string') {
                this.blockIndex2Name.push(obj);
                this.blockName2Index[obj] = i+1;
                // console.log(obj);
            } else {
                this.blockIndex2Name.push(obj[2]);
                this.blockName2Index[obj[2]] = i+1;
                // console.log(obj[2]);
            }
        }
        // console.log(this.blockIndex2Name, this.blockName2Index);
    }

}