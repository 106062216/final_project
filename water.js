"use strict";
exports.__esModule = true;
var voxelMesh = require('voxel-mesh');
var voxel = require('voxel');
var Water = /** @class */ (function () {
    function Water(opts) {
        this.game = opts.game;
        this.material = opts.material;
    }
    Water.prototype.generate = function (waterChunk) {
        var game = this.game;
        var scale = new game.THREE.Vector3(1, 1, 1);
        var water = voxelMesh(waterChunk, voxel.meshers.culled, scale, game.THREE);
        water.createSurfaceMesh(this.material);
        water.addToScene(game.scene);
    };
    return Water;
}());
exports.Water = Water;
