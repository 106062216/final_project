var createGame = require('voxel-engine');
var highlight = require('voxel-highlight');
var player = require('voxel-player');
var world = require('./world.js');
var fly = require('voxel-fly');
var walk = require('./walk.js');
var mine = require('./mining.js');
var glowstone = require('./glowStone.js');
var collide = require('./getCollide.js')
var Summon = require('./summon.js')

var opts = {
    texturePath: './assets/textures/',
    chunkSize: 16,
    chunkDistance: 2,
    fogScale: 16,
    lightsDisabled: true,
    generateChunks: false,
    materials: [
        ['grass', 'dirt', 'grass_dirt'], 'dirt', 'sand', 'stone', 'water', 'clouds', ['log_oak_top', 'log_oak_top', 'log_oak'], 'leaves_oak', 'cobblestone', 'glowstone', 'stone_granite', 'plank', 'wool_colored_blue', 'wool_colored_cyan', 'wool_colored_gray', 'wool_colored_light_blue', 'wool_colored_lime', 'wool_colored_magenta', 'wool_colored_orange', 'wool_colored_purple', 'wool_colored_red', 'wool_colored_silver', 'wool_colored_white', 'wool_colored_yellow', 'whitewool'
    ]
    // mesher: voxel.meshers.greedy
}

var game = createGame(opts);
var container = document.getElementById('container');
game.appendTo(container);

var createPlayer = player(game);
var dude = createPlayer('./assets/textures/shama.png');
dude.possess();
dude.yaw.position.set(0, 5, 0);

var creatRegistry = require('./registry');
var registry = new creatRegistry.Registry(opts.materials);
// console.log(registry.blockName2Index, registry.blockIndex2Name);

var weather = require('./weatherSystem');
weather(game);
var createzombie = require('voxel-dummy-player')(game)
Summon(game, collide, createzombie, dude);

var generateChunk = world();
var waterVoxels = {};
game.voxels.on('missingChunk', function (p) {
    var temp = generateChunk(p, game.chunkSize)
    var voxels = temp[0];
    waterVoxels[p] = temp[1];
    var chunk = {
        position: p,
        dims: [game.chunkSize, game.chunkSize, game.chunkSize],
        voxels: voxels
    }
    var waterChunk = {
        position: p,
        dims: [game.chunkSize, game.chunkSize, game.chunkSize],
        voxels: temp[1]
    }
    game.showChunk(chunk, 0)
    game.showChunk(waterChunk, 1)
})

var chunkNow = [0, 0, 0];
game.chunkRegion.on('change', function(pos) {
    chunkNow = pos;
    // console.log(waterVoxels[chunkNow]); 
})

function detectWater(){
    if (chunkNow === undefined || waterVoxels[chunkNow] === undefined) {
        return;
    }
    var x = Math.floor(dude.yaw.position.x);
    var y = Math.floor(dude.yaw.position.y);
    var z = Math.floor(dude.yaw.position.z);
    var xidx = Math.abs((game.chunkSize + x % game.chunkSize) % game.chunkSize)
    var yidx = Math.abs((game.chunkSize + y % game.chunkSize) % game.chunkSize)
    var zidx = Math.abs((game.chunkSize + z % game.chunkSize) % game.chunkSize)
    var idx = xidx + yidx * game.chunkSize + zidx * game.chunkSize * game.chunkSize
    if (waterVoxels[chunkNow][idx] === 5) {
        // console.log('hit water');
        document.getElementById('masks').style.display = 'unset';
    } else {
        document.getElementById('masks').style.display = 'none';
    }
    
}

var highlighter = highlight(game, {
    adjacentActive: function () {
        return game.controls.state.firealt
    }
});

mine(game, highlighter, container);
glowstone(game, highlighter, registry);

var currentMaterial = 1;
var showInv = false;
var currentitem = 1;
var skyFlag = true;
window.addEventListener('keydown', function (e) {
    if (e.keyCode === 'T'.charCodeAt(0)) dude.toggle();
    else if (e.keyCode === 'E'.charCodeAt(0)) {
        if (!showInv) {
            document.getElementById('inventory').style.display = 'unset';
            document.exitPointerLock();
        } else {
            document.getElementById('inventory').style.display = 'none';
            document.getElementById('container').requestPointerLock();
        }
        showInv = !showInv;
    }

    var tem = Number(e.key);
    if (tem > 0 && tem < 10) {
        currentitem = tem;
        var it = document.getElementById('item' + currentitem).src;
        var selection = document.getElementById('selection');
        selection.style = "width:40px; height: 100%; padding: 0%; position: relative; left:" + 11 * (currentitem - 1) + '%;';
        it = it.split("/");
        it = it[it.length - 1].replace(".png", "");
        currentMaterial = registry.blockName2Index[it];
        // console.log(currentMaterial);
    }
})

chooseInv = function (item) {
    var tem = item.src;
    document.getElementById('item' + currentitem).src = tem;
    tem = tem.split("/");
    tem = tem[tem.length-1].replace(".png", "");
    currentMaterial = registry.blockName2Index[tem];
    document.getElementById('inventory').style.display = 'none';
    document.getElementById('container').requestPointerLock();
    showInv = false;
}

container.addEventListener("mouseup", function (event) {
    
    if (event.button === 2) {
        var position = highlighter.currVoxelAdj;
        if (position) {
            game.createBlock(position, currentMaterial);
        }
    }
});

var createSky = require('voxel-sky')({
    game: game,
    // starting time of the day
    time: 0,
    // size of the sky
    size: game.worldWidth() * 2,
    // initial color of the sky
    color: new game.THREE.Color(0, 0, 0),
    // how fast the sky rotates
    speed: 1
});
var sky = createSky();
game.on('tick', sky);

window.onload = function () {
    document.getElementById("spinner").style = "display:none";
    game.paused = false;
};

var makeFly = fly(game)
var target = game.controls.target()
game.flyer = makeFly(target)

game.on('tick', function () {
    walk.render(target.playerSkin)
    var vx = Math.abs(target.velocity.x)
    var vz = Math.abs(target.velocity.z)
    if (vx > 0.001 || vz > 0.001) walk.stopWalking()
    else walk.startWalking()

    detectWater();
})


