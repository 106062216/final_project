
module.exports = BreakBlocks;

function BreakBlocks(game, highlighter, container) {

    var effect = new Audio("./assets/dig.mp3");
    effect.loop = true;
    effect.volume = 0.3;

    var cube = [];
    var eventNumber;
    var b = 0;
    var OriginPos;
    var CreateCube = function(src){
        const geometry = new game.THREE.CubeGeometry(1.01, 1.01, 1.01);
        const material = new game.THREE.MeshLambertMaterial({
            clipIntersection: true,
            transparent: true,
            precision: "highhp",
            clipShadows: true,
            opacity: 1
        });
        const cube = new game.THREE.Mesh(geometry, material);
        const image = new Image();

        image.onload = function (img) {

            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');

            canvas.width = 512;
            canvas.height = 512;

            ctx.drawImage(this, 0, 0, canvas.width, canvas.height);

            material.map = new game.THREE.Texture(canvas);
            material.map.needsUpdate = true;

        };

        image.crossOrigin = "anonymous";
        image.src = src;

        return cube;
    };

    async function createCubeGroup(){
        for (let i = 0 ; i < 10; i++) {
            cube[i] = await CreateCube('./assets/destroy/destroy_stage_'+i+'.png');
        }
    }
    createCubeGroup();

//using to debug
// window.addEventListener('keydown', function (ev) {
//     if (ev.which === 'R'.charCodeAt(0)) {
//         console.log("highlighter pos : ",highlighter.mesh.position);
//         console.log("highlighter scale : ",highlighter.mesh.scale);
//         var val = game.raycast().value;
//         console.log("block : ", val);
//     }
// });

    function once(fn, context) {
        var result;
        return function() {
            if(fn) {
                result = fn.apply(context || this, arguments);
                fn = null;
            }

            return result;
        };
    }

    function getPosition(){
        OriginPos = new game.THREE.Vector3(
            highlighter.mesh.position.x,
            highlighter.mesh.position.y,
            highlighter.mesh.position.z
        );
    }

    function hault(){
        clearInterval(eventNumber);
        game.scene.remove(cube[b-1]);
        b = 0;
        effect.pause();
    }

    function IMMstop(){
        if (OriginPos.x != highlighter.mesh.position.x || OriginPos.y != highlighter.mesh.position.y || OriginPos.z != highlighter.mesh.position.z) {
            hault();
            return;
        }
    }

    container.addEventListener("mouseup", function (event) {
        if (event.button === 0)
            hault();
    });

    container.addEventListener("mousemove", function (event) {
        if (OriginPos){
            if (OriginPos.x != highlighter.mesh.position.x || OriginPos.y != highlighter.mesh.position.y || OriginPos.z != highlighter.mesh.position.z) {
                hault();
                return;
            }
        }
    });

    container.addEventListener("mousedown" , function (event) {
        if(event.button === 0) {
            var getonce = once(getPosition());
            getonce();
            if(game.raycast().value !== undefined)
                effect.play();
            IMMstop();
            eventNumber = setInterval(function () {
                //console.log("dig " + b);
                if(b === 10){
                    game.scene.remove(cube[b-1]);
                    clearInterval(eventNumber);
                    var position = highlighter.currVoxelPos;
                    if(position) {
                        game.setBlock(position, 0);
                    }
                    effect.pause();
                    return;
                }
                cube[b].position = OriginPos;
                // To check whether is in the last State
                game.scene.remove(cube[b-1] ? cube[b-1] : null);
                game.scene.add(cube[b]);
                b++;
            }, 500)
        }
    });
}